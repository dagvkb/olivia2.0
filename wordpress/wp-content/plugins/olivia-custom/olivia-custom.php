<?php
/*
Plugin Name: Olivia Custom
Description: Custom functions and scripts for Olivia website
Version: 1.0
Author: Superblaise
Author URI: http://superblaise.no/
Text Domain: olivia-custom
License: GPL2
*/

/**
 * Prepare categorized restaurant menu output
 */
add_filter( 'acf/rest_api/restaurants/get_fields', function( $data ) {

	if ( !array_key_exists( 'restaurant_meals', $data['acf'])) {
		return $data;
	}
	// Get all terms of meals_cat taxonomy
	$categories = get_terms( 'meals_cat', 'orderby=term_order&hide_empty=0' );
	$meals_categorized = array();
	// Create empty arrays on category name keys
	foreach ($categories as $key => $category) {
		$meals_categorized[$category->name] = array();
	}
	// Loop through all meals and append them to corresponding cat
	foreach ($data['acf']['restaurant_meals'] as $key => $meal) {
		$meal_categories = get_the_terms( $meal->ID, 'meals_cat' );
		$meal_allergens = get_the_terms( $meal->ID, 'allergens' );
		$meal->meal_price = get_field( 'meal_price', $meal->ID );

		if ( $meal_allergens ) {
			$meal->meal_allergens = implode(', ', array_map(function($c) {
			    return $c->name;
			}, $meal_allergens));
		}

		if ( $meal_categories ) {
			foreach ($meal_categories as $term) {
				if ( !array_key_exists( $term->name, $meals_categorized ) ) {
					$meals_categorized[$term->name] = array();
				}
				array_push( $meals_categorized[$term->name], $meal);
			}
		}
	}
    $data['acf']['restaurant_meals_categorized'] = array_filter($meals_categorized);

    /**
     * Shared restaurant meals
     */
    // Check if shared meals exist at all
	if ( !array_key_exists( 'shared_restaurant_meals', $data['acf']) || empty($data['acf']['shared_restaurant_meals']) ) {
		return $data;
	}
	// Get all terms of shared_menu_name taxonomy
	$shared_categories = get_terms( 'shared_menu_name', 'orderby=term_order&hide_empty=0' );
	$visible_categories = get_field('select_shared_menus');

	$categories_with_keys = array();
	foreach ($categories as $key => $category) {
		$categories_with_keys[$category->name] = array(
			'items' => array(),
			'meal_category_description_for_shared_meals' => get_field('meal_category_description_for_shared_meals', $category)
		);
	}

	$shared_meals_categorized = array();
	// Create empty arrays on category name keys
	foreach ($shared_categories as $key => $category) {
		$shared_meals_categorized[$category->name] = array(
			'items' => $categories_with_keys,
			'description' => $category->description,
			'shared_meals_price' => get_field('shared_meals_price', $category)
		);
	}

	foreach ($data['acf']['shared_restaurant_meals'] as $key => $meal) {
		$shared_menu_name = get_the_terms( $meal->ID, 'shared_menu_name' );
		$meal_categories = get_the_terms( $meal->ID, 'meals_cat' );
		$meal_allergens = get_the_terms( $meal->ID, 'allergens' );
		$meal->meal_price = get_field( 'meal_price', $meal->ID );

		if ( $meal_allergens ) {
			$meal->meal_allergens = implode(', ', array_map(function($c) {
			    return $c->name;
			}, $meal_allergens));
		}

		if ( $shared_menu_name && $meal_categories ) {
			foreach ($shared_menu_name as $shared_term) {
				if ( !array_key_exists( $shared_term->name, $shared_meals_categorized ) ) {
					$shared_meals_categorized[$shared_term->name] = array(
						'items' => $categories_with_keys,
						'description' => $category->description,
						'shared_meals_price' => get_field('shared_meals_price', $category)
					);
				}

				foreach ($meal_categories as $term) {
					if ( !array_key_exists( $term->name, $shared_meals_categorized[$shared_term->name]['items'] ) ) {
						$shared_meals_categorized[$shared_term->name]['items'][$term->name] = array(
							'items' => array(),
							'meal_category_description_for_shared_meals' => get_field('meal_category_description_for_shared_meals', $term)
						);
					}
					array_push( $shared_meals_categorized[$shared_term->name]['items'][$term->name]['items'], $meal );
				}

			}
		}
	}

	// Remove empty categories
	foreach ($shared_meals_categorized as $key => $shared_cat_data) {
		foreach ($shared_cat_data['items'] as $shared_key => $shared_value) {
			if (empty($shared_meals_categorized[$key]['items'][$shared_key]['items'])) {
				unset($shared_meals_categorized[$key]['items'][$shared_key]);
			}
		}

		if (empty($shared_meals_categorized[$key]['items'])) {
			unset($shared_meals_categorized[$key]);
			continue;
		}
	}

	// Remove shared menus which were not chosen
	if ($visible_categories) {
		foreach ($shared_meals_categorized as $shared_key => $shared_cat_data) {
			$cat_exists = false;

			foreach ($visible_categories as $visible_key => $category) {
				$curr_cat_name = $category->name;

				if ($shared_key == $curr_cat_name) {
					$cat_exists = true;
				}
			}

			if (!$cat_exists) {
				unset($shared_meals_categorized[$shared_key]);
			}
		}
	}

	$data['acf']['restaurant_shared_meals_categorized'] = $shared_meals_categorized;

    return $data;

} );

/**
 * Add restaurant header_title and header_subtitle to page fetch
 */
add_filter( 'acf/rest_api/page/get_fields', function( $data ) {
	if ( array_key_exists( 'restaurant_list', $data['acf']) ) {
		foreach ($data['acf']['restaurant_list'] as $key => $restaurant) {
			$data['acf']['restaurant_list'][$key]->header_title = get_field( 'header_title', $restaurant->ID );
			$data['acf']['restaurant_list'][$key]->header_subtitle = get_field( 'header_subtitle', $restaurant->ID );
			$data['acf']['restaurant_list'][$key]->restaurant_city = get_field( 'restaurant_city', $restaurant->ID );
		}
	}

	if ( array_key_exists( 'vacancies', $data['acf']) ) {
		$format = "d/m/Y";
		$today  = DateTime::createFromFormat($format, date('d/m/Y'));

		$valid_vacancies = array();

		foreach ($data['acf']['vacancies'] as $key => $vacancy) {
			$vacancy_date = $data['acf']['vacancies'][$key]['closing_date'];
			if ($vacancy_date == "" || !$vacancy_date) {
				continue;
			}

			$vacancy_date  = DateTime::createFromFormat($format, $vacancy_date);

			if ( $today < $vacancy_date ){
				array_push( $valid_vacancies, $data['acf']['vacancies'][$key]);
			}
		}

		$data['acf']['vacancies'] = $valid_vacancies;
	}

	$data['acf']['tester'] = 'tester';

    return $data;
} );

/**
 * Add custom fields to globalData js object
 */
add_filter( 'acf/rest_api/option/get_fields', function( $data ) {
	if ( array_key_exists( 'restaurant_list', $data['acf']) ) {
		foreach ($data['acf']['restaurant_list'] as $key => $restaurant) {
			$data['acf']['restaurant_list'][$key]->header_title = get_field( 'header_title', $restaurant->ID );
			$data['acf']['restaurant_list'][$key]->header_subtitle = get_field( 'header_subtitle', $restaurant->ID );
			$data['acf']['restaurant_list'][$key]->restaurant_city = get_field( 'restaurant_city', $restaurant->ID );
		}
	}

    return $data;
} );
