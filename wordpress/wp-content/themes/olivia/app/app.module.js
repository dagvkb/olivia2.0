angular.module('app', [
	'ui.router',
    'app.routes',
    'app.controllers',
    'app.services',
    'app.directives',
    'vAccordion',
    'ngAnimate',
    'ui.select',
    'ngSanitize',
    'duScroll',
    'nemLogging',
	'uiGmapgoogle-maps',
    'hl.sticky',
    'xml'
])
.config(function(uiGmapGoogleMapApiProvider, x2jsProvider) {
	uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyA7u559I2e7lmwl_N8dhOguehmXhAgQN_s',
        libraries: 'weather,geometry,visualization'
    });

    x2jsProvider.config = {
      /*
      escapeMode               : true|false - Escaping XML characters. Default is true from v1.1.0+
      attributePrefix          : "<string>" - Prefix for XML attributes in JSon model. Default is "_"
      arrayAccessForm          : "none"|"property" - The array access form (none|property). Use this property if you want X2JS generates an additional property <element>_asArray to access in array form for any XML element. Default is none from v1.1.0+
      emptyNodeForm            : "text"|"object" - Handling empty nodes (text|object) mode. When X2JS found empty node like <test></test> it will be transformed to test : '' for 'text' mode, or to Object for 'object' mode. Default is 'text'
      enableToStringFunc       : true|false - Enable/disable an auxiliary function in generated JSON objects to print text nodes with text/cdata. Default is true
      arrayAccessFormPaths     : [] - Array access paths. Use this option to configure paths to XML elements always in "array form". You can configure beforehand paths to all your array elements based on XSD or your knowledge. Every path could be a simple string (like 'parent.child1.child2'), a regex (like /.*\.child2/), or a custom function. Default is empty
      skipEmptyTextNodesForObj : true|false - Skip empty text tags for nodes with children. Default is true.
      stripWhitespaces         : true|false - Strip whitespaces (trimming text nodes). Default is true.
      datetimeAccessFormPaths  : [] - Datetime access paths. Use this option to configure paths to XML elements for "datetime form". You can configure beforehand paths to all your array elements based on XSD or your knowledge. Every path could be a simple string (like 'parent.child1.child2'), a regex (like /.*\.child2/), or a custom function. Default is empty
      */
    };
})

.run(function($rootScope, globalService, $location) {
	var hostname = location.port ? location.hostname + ':' + location.port : location.hostname;
	$rootScope.$urlPrefix = 'http://' + hostname + '/wp-json';


  $rootScope.$on('$stateChangeSuccess', function (event, toState) {
      console.log('$location.path()', $location.path());
      window.dataLayer.push({
          event: 'pageView',
          action: $location.path()
      });
  });

  // Facebook Pixel
  $rootScope.$on("$locationChangeSuccess", function(event, next, current) {

    setTimeout(function() {

      // preventing double registration when URL is changing
      if (next === 'http://' + hostname + '/') {
        return false;
      } else {
        var url = location.hash;
        window.fbq('track', 'ViewContent', { url });
        }
    }, 500)

  });



	globalService.loadGlobalData().then(function() {
		$rootScope.globalData = globalService.getGlobalData();

	}, function(reason) {
		console.log('Unable to fetch global data. Reson:', reason);
	});

	$rootScope.$on('$stateChangeSuccess', function() {
	   document.body.scrollTop = document.documentElement.scrollTop = 0;
	});


});
