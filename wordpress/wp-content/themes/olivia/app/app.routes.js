angular.module('app.routes', [])
.config(function($stateProvider, $urlRouterProvider, $locationProvider, $windowProvider) {


  // facebook pixel, tracking only landingpage
  setTimeout(function() {
    var $window = $windowProvider.$get();
    var url = $window.location.hash;
    $window.fbq('track', 'PageView', {url});
  }, 500)

  $stateProvider

  .state('app', {
    url: '/home',
    templateUrl: olivia.partials + '/home/home.view.html',
    controller: 'homeCtrl'
  })

  .state('about', {
    url: '/about-olivia',
    templateUrl: olivia.partials + '/about/about.view.html',
    controller: 'aboutCtrl'
  })

  .state('careers', {
    url: '/careers',
    templateUrl: olivia.partials + '/careers/careers.view.html',
    controller: 'careersCtrl'
  })

  .state('english', {
    url: '/english',
    templateUrl: olivia.partials + '/english/english.view.html',
    controller: 'englishCtrl'
  })

  .state('restaurant', {
    url: '/restaurant/:restaurantId',
    templateUrl: olivia.partials + '/restaurant/restaurant.view.html',
    controller: 'restaurantCtrl'
  })

  .state('bookTable', {
    url: '/book-table/:restaurantId',
    templateUrl: olivia.partials + '/book-table/book-table.view.html',
    controller: 'bookTableCtrl'
  })

	$urlRouterProvider.otherwise('/home')

});
