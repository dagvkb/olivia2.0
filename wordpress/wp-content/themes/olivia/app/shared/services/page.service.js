angular.module('app.services')

.factory('pageService',function($rootScope, $http, $filter){
	var service = {};
	service.pageData = {};

	/**
	 * Load page data
	 * @param  {int} id Page id which needs to be fetched
	 * @return {promise} Promise whether data fetching was successfull
	 */
	service.loadPageData = function(id) {
		if (isNaN(id)) {
			return false;
		}

		var _this = this;

	    return $http({
	      method: 'GET',
	      url: $rootScope.$urlPrefix + '/wp/v2/pages/' + id
	    }).then(function(response) {
	    	// Check if response is a single object containing page id
	    	if (!response.data.id) {
	    		return false;
	    	}
	    	_this.pageData[id] = response.data;
	    });
	}

	/**
	 * Retrieve page data
	 * @param  {int} id Page id which needs to be fetched
	 * @return {object} Page data object
	 */
	service.getPageData = function(id) {
		if (isNaN(id)) {
			return false;
		}

		var _this = this;
		if (typeof this.pageData[id] === 'undefined') {
			this.loadPageData(id).then(function() {
				return _this.pageData[id];
			});
		} else {
			return _this.pageData[id];
		}
	}

	return service;
});
