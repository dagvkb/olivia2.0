angular.module('app.services')

.factory('globalService',function($rootScope, $http, $filter){
	var service = {};
	service.globalData = {};

	service.loadGlobalData = function() {
		var _this = this,
			id = 'options';

	    return $http({
	      method: 'GET',
	      url: $rootScope.$urlPrefix + '/acf/v3/options/' + id
	    }).then(function(response) {
	    	// Check if response is a single object containing page id
	    	if (typeof response.data.acf === 'undefined') {
	    		return false;
	    	}
	    	_this.globalData = response.data.acf;
	    });
	}

	service.getGlobalData = function() {
		var _this = this;
		if (typeof this.globalData.logo === 'undefined') {
			this.loadGlobalData().then(function() {
				return _this.globalData;
			});
		}
		else {
			return _this.globalData;
		}
	}

	return service;
})
