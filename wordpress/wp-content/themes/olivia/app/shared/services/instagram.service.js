angular.module('app.services')

.factory('InstagramAPI', function($http, $sce) {
	return {
      fetchPhotos : function(callback) {

		let client_id = '74338fd20b58498a8091de5c7819f8ab';
		let user_id = '355887609';
		let access_token = '355887609.3a81a9f.db58bce0c8244b8a8761ba67c4ba9452';

        let endpoint = 'https://api.instagram.com/v1/users/';
        endpoint += user_id;
        endpoint += '/media/recent/?';
        endpoint += '?count=99';
        endpoint += '&access_token=' + access_token;

        let trustUrl = $sce.trustAsResourceUrl(endpoint);

        $http.jsonp(trustUrl, {jsonpCallbackParam: 'callback'})
        .then(function(response) {
          callback(response.data);
        }, function(xhr, status, err) {
          console.error(status, err);
        })
      }
    }
});
