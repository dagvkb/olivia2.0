angular.module('app.controllers')

.controller('footerCtrl', ['$rootScope', '$scope', '$stateParams', '$state', '$http', 'globalService',
function ($rootScope, $scope, $stateParams, $state, $http, globalService) {
	$scope.restaurantId = false;
	$scope.submenuVisible = false;

	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		if (typeof $stateParams.restaurantId !== 'undefined') {
			$scope.restaurantId = $stateParams.restaurantId;
		}
		$scope.submenuVisible = $state.includes("restaurant");
	})
}])
