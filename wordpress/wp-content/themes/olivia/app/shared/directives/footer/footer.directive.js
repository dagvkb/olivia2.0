angular.module('app.directives')

.directive('oliviaFooter', function() {
  return {
    restrict: 'AE',
    templateUrl: olivia.shared + '/directives/footer/footer.view.html',
    controller: 'footerCtrl'
  };
})
