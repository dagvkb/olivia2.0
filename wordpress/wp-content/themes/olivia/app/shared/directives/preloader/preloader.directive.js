angular.module('app.directives')

.directive('preloader', function($http) {
	return {
		restrict: 'AE',
		templateUrl: olivia.shared + '/directives/preloader/preloader.view.html',
		link: function($scope, $elem, attrs) {
			$scope.animate = true;

			$scope.loading = function() {
				return $http.pendingRequests.length;
			};

			$scope.$watch($scope.loading, function(ajax) {
				if (ajax) {
					$scope.animate = true;
				} else {
					$scope.animate = false;
				}
			});

		}
	};
})
