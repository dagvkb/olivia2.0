angular.module('app.directives')

.directive('oliviaHeader', function() {
  return {
    restrict: 'AE',
    templateUrl: olivia.shared + '/directives/header/header.view.html',
    controller: 'headerCtrl'
  };
})
