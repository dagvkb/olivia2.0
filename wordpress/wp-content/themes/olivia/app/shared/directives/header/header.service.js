angular.module('app.services')

.factory('headerService',function($rootScope, $http, $filter){
	var service = {};
	service.restaurantList = {};

	service.loadRestaurantList = function() {
		var _this = this,
			id = 'restaurants';

	    return $http({
	      method: 'GET',
	      url: $rootScope.$urlPrefix + '/wp/v2/' + id
	    }).then(function(response) {
	    	// Check if response is a single object containing page id
	    	if (typeof response.data === 'undefined') {
	    		return false;
	    	}
	    	_this.restaurantList = response.data;
	    });
	}

	service.getRestaurantList = function() {
		var _this = this;
		if (typeof this.restaurantList === 'undefined') {
			this.loadrestaurantList().then(function() {
				return _this.restaurantList;
			});
		}
		else {
			return _this.restaurantList;
		}
	}

	return service;
});
