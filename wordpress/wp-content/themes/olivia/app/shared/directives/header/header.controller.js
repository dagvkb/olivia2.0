angular.module('app.controllers')

.controller('headerCtrl', ['$scope', '$stateParams', '$http', 'headerService', '$state', '$timeout',
function ($scope, $stateParams, $http, headerService, $state, $timeout) {
	$scope.restaurantList = {};
	// Initialize chosen restaurant obj
	$scope.restaurant = {
		selected: undefined
	};
	// Timeout needed in order to wait for .run function to be finished
	$timeout(function () {
		$scope.actions();
	}, 100);

	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		$scope.actions();
		$scope.resetChosenRestaurant();
	});

	headerService.loadRestaurantList().then(function() {
		$scope.restaurantList = headerService.getRestaurantList();

		$scope.resetChosenRestaurant();
	}, function(reason) {
		console.log('Unable to fetch about restaurant list data. Reson:', reason);
	});


	/**
	 * Reset chosen restaurant dropdown if restaurant state not active
	 */
	$scope.resetChosenRestaurant = function() {
		if (typeof $scope.restaurantList === 'undefined') {
			return false;
		}

		if (typeof $stateParams.restaurantId === 'undefined') {
			$scope.restaurant.selected = undefined;
			return false;
		}

		var restaurantId = $stateParams.restaurantId;
		for (var index in $scope.restaurantList) {
			var restaurant = $scope.restaurantList[index];

			if (restaurant.slug == restaurantId) {
				$scope.restaurant.selected = $scope.restaurantList[index];
			}
		}
	}

	/**
	 * Actions performed on successful state change
	 */
	$scope.actions = function() {
		$scope.submenu = {
			visible: false,
			items: []
		}

		if ($state.includes("restaurant")) {
			$scope.submenu = $scope.restaurantSubmenu;
		}

		if ($state.includes("about")) {
			$scope.submenu = $scope.aboutSubmenu;
		}

		if ($state.includes("careers")) {
			$scope.submenu = $scope.careersSubmenu;
		}

		if ($state.includes("english")) {
			$scope.submenu = $scope.englishSubmenu;
		}

		if ($state.includes("bookTable")) {
			$scope.submenu = $scope.submenu;
		}

		$scope.mobileMenuOpened = false;
		toggleBodyScroll($scope.mobileMenuOpened);

	}

	/**
	 * Mobile menu functionality
	 */
	$scope.mobileMenuOpened = false;
	$scope.toggleMobileMenu = function() {
		$scope.mobileMenuOpened = !$scope.mobileMenuOpened;
		toggleBodyScroll($scope.mobileMenuOpened);
	}

	/**
	 * Disable body scroll when mobile menu is open
	 */
	 function toggleBodyScroll(open) {
		var body = document.body;
		if (open) {
			 body.classList.add('noscroll');
		} else {
			body.classList.remove('noscroll');
		}
	 }


	/**
	 * Submenu
	 */
	$scope.restaurantSubmenu = {
		visible: true,
		items: [
			{
				label: 'Meny',
				href: '#menu'
			},
			{
				label: 'Om restauranten',
				href: '#about'
			},
			{
				label: 'Kontakt',
				href: '#contact-info'
			}
		]
	}

	$scope.aboutSubmenu = {
		visible: true,
		items: [
			{
				label: 'Gavekort',
				href: '#gift-card'
			},
			{
				label: 'Sala privata',
				href: '#events'
			},
			{
				label: 'Restauranter',
				href: '#restaurants'
			}
		]
	}

	$scope.careersSubmenu = {
		visible: true,
		items: [
			{
				label: 'Ledige stillinger',
				href: '#vacancies'
			},
			{
				label: 'Vår familie',
				href: '#our-team'
			}
		]
	}

	$scope.englishSubmenu = {
		visible: true,
		items: [
			{
				label: 'Info',
				href: '#info'
			},
			{
				label: 'Restaurants',
				href: '#restaurants'
			}
		]
	}


}])
