angular.module('app.controllers')

.controller('careersCtrl', ['$scope', '$stateParams', '$http', 'pageService', 'careersService', '$sce',
function ($scope, $stateParams, $http, pageService, careersService, $sce) {

	$scope.pageId = 83; // 737
	$scope.activeIndex = false;
	$scope.openCareers = [];

	pageService.loadPageData($scope.pageId).then(function() {
		$scope.careersData = pageService.getPageData($scope.pageId);

        // find the length of the gallery to send to DOM
        if ($scope.careersData.acf.ken_burns_image_gallery) {
                $scope.galleryArrLength = $scope.careersData.acf.gallery.length;
        }

        $scope.frontImage = $scope.careersData.acf.header_background_image.url;
        $scope.frontImageAlt = $scope.careersData.acf.header_background_image.title;
        $scope.vacancies = $scope.careersData.acf.vacancies;
        $scope.oliviaSeparator = $scope.careersData.acf.olivia_separator_after_section;

        $scope.section_title = $sce.trustAsHtml($scope.careersData.acf.section_title);
        $scope.section_content = $sce.trustAsHtml($scope.careersData.acf.section_content);
        $scope.content_below_images = $sce.trustAsHtml($scope.careersData.acf.content_below_images);
        $scope.video = $sce.trustAsHtml($scope.careersData.acf.video);
	}, function(reason) {
		console.log('Unable to fetch careers page data. Reson:', reason);
	});

	careersService.loadCareers().then(function() {
		var openCareers = careersService.getCareers();

		if (openCareers instanceof Array) {
			$scope.openCareers = openCareers;
		} else {
			if (openCareers !== '' && openCareers !== null && typeof openCareers !== 'undefined' && openCareers instanceof Object) {
				$scope.openCareers.push(openCareers);
			}
		}
	}, function(reason) {
		console.log('Unable to fetch careers data. Reson:', reason);
		$state.go('app');
	});


	$scope.toggleCareer = function(careerIndex) {
		if (careerIndex == 'close') {
			$scope.activeIndex = false;
			return false;
		}

		if (typeof $scope.openCareers[careerIndex] === 'undefined') {
			return false;
		}

		$scope.activeIndex = careerIndex;
	}
}])

