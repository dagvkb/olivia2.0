angular.module('app.services')

.factory('careersService',function($rootScope, $http, $filter){
	var service = {};
	service.careers = {};

	service.loadCareers = function() {
		var _this = this;
		var reachmeeURL = 'https://site106.reachmee.com/Public/rssfeed/external.ashx?id=6&InstallationID=I012&CustomerName=olivia&lang=NO';

		return $http.get(reachmeeURL).then(function (data) {
			if (typeof data === 'undefined' || typeof data.data === 'undefined') {
				console.log('Not a valid Reachmee response!');
				return false;
			}

			var x2js = new X2JS();
			var jsonObj = x2js.xml2js(data.data);

			if (typeof jsonObj.rss === 'undefined' || typeof jsonObj.rss.channel === 'undefined' || typeof jsonObj.rss.channel.item === 'undefined') {
				console.log('Not a valid Reachmee response!');
			}

			_this.careers = jsonObj.rss.channel.item;
	    }, function(data) {
			console.log('Unable to fetch Reachmee data!', data);
	    });
	}

	service.getCareers = function() {
		var _this = this;
		if (typeof this.careers === 'undefined') {
			this.loadCareers().then(function() {
				return _this.careers;
			});
		}
		else {
			return _this.careers;
		}
	}

	return service;
});
