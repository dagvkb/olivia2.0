angular.module('app.controllers')

.controller('bookTableCtrl', ['$scope', '$stateParams', '$http', 'bookTableService', 'pageService', '$sce',
function ($scope, $stateParams, $http, bookTableService, pageService, $sce) {

    $scope.pageSlug = $stateParams.restaurantId;
    bookTableService.loadRestaurantData($scope.pageSlug).then(function() {
        $scope.restaurantData = bookTableService.getRestaurantData($scope.pageSlug);

        $scope.bookatableId = $sce.trustAsResourceUrl('https://olivia.2book.se/public/' + $scope.restaurantData.acf.bookatable_id);
    }, function(error) {
        console.log('Error because -->', error );
    });

    $scope.pageId = 456;
	if (location.hostname !== "localhost") {
		$scope.pageId = 456;
	}
	pageService.loadPageData($scope.pageId).then(function() {
		$scope.bookData = pageService.getPageData($scope.pageId);
	}, function(reason) {
		console.log('Unable to fetch about page data. Reson:', reason);
	});

}])
