angular.module('app.controllers')

.controller('englishCtrl', ['$scope', '$stateParams', '$http', 'pageService', '$sce',
    function($scope, $stateParams, $http, pageService, $sce) {

        $scope.pageId = 557;
        if (location.hostname !== 'localhost') {
            $scope.pageId = 557;
        }

        pageService.loadPageData($scope.pageId).then(function() {
            $scope.englishData = pageService.getPageData($scope.pageId);

            // find the length of the gallery to send to DOM
            if ($scope.englishData.acf.ken_burns_image_gallery) {
                $scope.galleryArrLength = $scope.englishData.acf.gallery.length;
            }

            $scope.restaurants = $scope.englishData.acf.restaurant_list;

        }, function(reason) {
            console.log('Unable to fetch english page data. Reason:', reason);
        })

    }])