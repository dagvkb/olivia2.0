angular.module('app.controllers')

.controller('homeCtrl', ['$scope', '$stateParams', '$http', 'pageService', 'InstagramAPI',
function ($scope, $stateParams, $http, pageService, InstagramAPI) {
	$scope.pageId = 60; //735

	pageService.loadPageData($scope.pageId).then(function() {
		$scope.homeData = pageService.getPageData($scope.pageId);


        // find the length of the gallery to send to DOM
        if ($scope.homeData.acf.ken_burns_image_gallery) {
            $scope.galleryArrLength = $scope.homeData.acf.gallery.length;
        }

	    $scope.frontImage = $scope.homeData.acf.header_background_image.url;
	    $scope.oliviaFamilyImage = $scope.homeData.acf.family_background_image.url;
	    $scope.oliviaFamilyImageAlt = $scope.homeData.acf.family_background_image.title;
	    $scope.restaurants = $scope.homeData.acf.restaurant_list;

	}, function(reason) {
		console.log('Unable to fetch homepage data. Reson:', reason);
	});




	$scope.layout = 'grid';
    $scope.data = {};
    $scope.instagramPictures = [];

    InstagramAPI.fetchPhotos(function(data) {
    	$scope.instagramPictures = data.data;
    });

    $scope.isOdd = function(n) {
	   return Math.abs(n % 2) == 1;
	}






}])
