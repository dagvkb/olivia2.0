angular.module('app.controllers')

.controller('aboutCtrl', ['$scope', '$stateParams', '$http', 'pageService', '$sce',
function ($scope, $stateParams, $http, pageService, $sce) {
	$scope.pageId = 85; //738

	pageService.loadPageData($scope.pageId).then(function() {
		$scope.aboutData = pageService.getPageData($scope.pageId);

        // find the length of the gallery to send to DOM
        if ($scope.aboutData.acf.ken_burns_image_gallery) {
            $scope.galleryArrLength = $scope.aboutData.acf.gallery.length;
        }

        $scope.frontImage = $scope.aboutData.acf.header_background_image.url;
        $scope.frontImageAlt = $scope.aboutData.acf.header_background_image.title;
        $scope.fullImage = $scope.aboutData.acf.full_image.url;

        $scope.restaurants = $scope.aboutData.acf.restaurant_list;

        $scope.intro_section_content = $sce.trustAsHtml($scope.aboutData.acf.intro_section_content);
        $scope.content_below_images = $sce.trustAsHtml($scope.aboutData.acf.content_below_images);
        $scope.gift_section_content = $sce.trustAsHtml($scope.aboutData.acf.gift_section_content);
        $scope.banquet_section_content = $sce.trustAsHtml($scope.aboutData.acf.banquet_section_content);
        $scope.cookies_section_content = $sce.trustAsHtml($scope.aboutData.acf.cookies_section_content);
	}, function(reason) {
		console.log('Unable to fetch about page data. Reson:', reason);
	});

    $scope.isOdd = function(n) {
	   return Math.abs(n % 2) == 1;
	}
}])
