angular.module('app.services')

.factory('restaurantService',function($rootScope, $http, $filter){
	var service = {};
	service.restaurantData = {};

	/**
	 * Load restaurant data
	 * @param  {int} id Restaurant id which needs to be fetched
	 * @return {promise} Promise whether data fetching was successfull
	 */
	service.loadRestaurantData = function(slug) {
		var _this = this;

	    return $http({
	      method: 'GET',
	      url: $rootScope.$urlPrefix + '/wp/v2/restaurants/?slug=' + slug
	    }).then(function(response) {
	    	// Check if response is a single object containing restaurant post slug
	    	if (typeof response.data[0] === 'undefined' || !response.data[0].slug) {
	    		return false;
	    	}
	    	_this.restaurantData[slug] = response.data[0];
	    });
	}

	/**
	 * Retrieve retaurant data
	 * @param  {string} slug Restaurant slug which needs to be fetched
	 * @return {object} Restaurant data object
	 */
	service.getRestaurantData = function(slug) {
		var _this = this;
		if (typeof this.restaurantData[slug] === 'undefined') {
			this.loadRestaurantData(slug).then(function() {
				return _this.restaurantData[slug];
			});
		}

		return _this.restaurantData[slug];
	}

	return service;
});
