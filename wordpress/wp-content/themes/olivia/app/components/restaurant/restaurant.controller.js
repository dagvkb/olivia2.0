angular.module('app.controllers')

.controller('restaurantCtrl', ['$scope', '$stateParams', '$http', 'restaurantService', '$state', '$timeout', '$sce',
function ($scope, $stateParams, $http, restaurantService, $state, $timeout, $sce) {
	$scope.pageSlug = $stateParams.restaurantId;
	$scope.mapStyle = [
    {
        "featureType": "all",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "saturation": "0"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": "0"
            },
            {
                "color": "#867a76"
            },
            {
                "lightness": "0"
            },
            {
                "gamma": "1"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2c2a28"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#262422"
            },
            {
                "visibility": "on"
            },
            {
                "lightness": "0"
            },
            {
                "gamma": "0.5"
            },
            {
                "weight": "0"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#867a76"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#262422"
            },
            {
                "gamma": "10.00"
            },
            {
                "lightness": "-70"
            },
            {
                "weight": "2"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#867a76"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2c2a28"
            },
            {
                "lightness": "-13"
            },
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#a95a5a"
            },
            {
                "saturation": "-69"
            },
            {
                "gamma": "5.43"
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#201616"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#2c2a28"
            },
            {
                "lightness": "2"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2c2a28"
            },
            {
                "lightness": "-4"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#262422"
            },
            {
                "lightness": "0"
            },
            {
                "gamma": "1.3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#a39992"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#262422"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#252220"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#262422"
            },
            {
                "gamma": "0.92"
            }
        ]
    }
];

	restaurantService.loadRestaurantData($scope.pageSlug).then(function() {
		$scope.restaurantData = restaurantService.getRestaurantData($scope.pageSlug);

		if (typeof $scope.restaurantData === 'undefined') {
			$state.go('app');
			return false;
		}


        // find the length of the gallery to send to DOM
        if ($scope.restaurantData.acf.ken_burns_image_gallery) {
            $scope.galleryArrLength = $scope.restaurantData.acf.gallery.length;
        }

		$scope.frontImage = $scope.restaurantData.acf.header_background_image.url;
		$scope.meals = $scope.restaurantData.acf.restaurant_meals_categorized;
        $scope.sharedMeals = $scope.restaurantData.acf.restaurant_shared_meals_categorized;

		// Google streetview
		$scope.streetview = $sce.trustAsHtml($scope.restaurantData.acf.restaurant_streetview);

		// Used by Google maps
		var lat = parseFloat($scope.restaurantData.acf.restaurant_location.lat);
		var lng = parseFloat($scope.restaurantData.acf.restaurant_location.lng);

		$scope.map = {
			center: {
				latitude: lat,
				longitude: lng
			},
			zoom: 17,
			options: {
				styles: $scope.mapStyle,
				scrollwheel: false
			}
		};
		$scope.marker = {
			id: 0,
			options: {
				icon: '/wp-content/themes/olivia/img/icons/olivia-marker.png'
			},
			coords: {
				latitude: lat,
				longitude: lng
			}
		};
	}, function(reason) {
		console.log('Unable to fetch restaurant data. Reson:', reason);
		$state.go('app');
	});

	$scope.accordionLabel = function($pane) {
		var label = '';
		if ($pane.isExpanded()) {
			label = 'SE MINDRE';
		} else {
			label = 'SE MER';
		}

		return label;
	}
}])
