<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package olivia
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function olivia_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'olivia_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function olivia_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'olivia_pingback_header' );

/**
 * Allow uploading of svg files
 * @param array $file_types Existing file types
 */
function add_file_types_to_uploads($file_types){

    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );

    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

/**
 * Add ACF Options page
 */
if( function_exists('acf_add_options_page') ) {
	$args = array(
		/* (string) The title displayed on the options page. Required. */
		'page_title' => 'Theme Options',

		/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
		'menu_title' => '',

		/* (string) The slug name to refer to this menu by (should be unique for this menu).
		Defaults to a url friendly version of menu_slug */
		'menu_slug' => '',

		/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
		Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
		'capability' => 'edit_posts',

		/* (int|string) The position in the menu order this menu should appear.
		WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
		Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
		Defaults to bottom of utility menu items */
		'position' => false,

		/* (string) The slug of another WP admin page. if set, this will become a child page. */
		'parent_slug' => 'themes.php',

		/* (string) The icon class for this menu. Defaults to default WordPress gear.
		Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
		'icon_url' => false,

		/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
		If set to false, this parent page will appear alongside any child pages. Defaults to true */
		'redirect' => true,

		/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
		Defaults to 'options'. Added in v5.2.7 */
		'post_id' => 'options',

		/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
		Defaults to false. Added in v5.2.8. */
		'autoload' => false,

	);
	acf_add_options_page( $args );
}
