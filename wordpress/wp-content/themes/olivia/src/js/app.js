/**
 * Angular core modules
 */
require('angular')
require('angular-ui-router');
require('angular-animate');
require('angular-sanitize');

/**
 * Main app module and routes
 */
require('../../app/app.module.js');
require('../../app/app.routes.js');

/**
 * Controllers
 */
require('../../app/shared/controllers/init.controller.js');
require('../../app/shared/directives/header/header.controller.js');
require('../../app/shared/directives/footer/footer.controller.js');
require('../../app/components/home/home.controller.js');
require('../../app/components/about/about.controller.js');
require('../../app/components/careers/careers.controller.js');
require('../../app/components/restaurant/restaurant.controller.js');
require('../../app/components/book-table/book-table.controller.js');
require('../../app/components/english/english.controller.js');

/**
 * Services
 */
require('../../app/shared/services/init.service.js');
require('../../app/shared/services/global.service.js');
require('../../app/shared/directives//header/header.service.js');
require('../../app/components/home/home.service.js');
require('../../app/components/about/about.service.js');
require('../../app/components/careers/careers.service.js');
require('../../app/components/restaurant/restaurant.service.js');
require('../../app/components/book-table/book-table.service.js');
require('../../app/components/english/english.service.js');
require('../../app/shared/services/instagram.service.js');
require('../../app/shared/services/page.service.js');

/**
 * Directives
 */
require('../../app/shared/directives/init.directive.js');
require('../../app/shared/directives/header/header.directive.js');
require('../../app/shared/directives/footer/footer.directive.js');
require('../../app/shared/directives/preloader/preloader.directive.js');

/**
 * 3rd party dependencies
 */
require('ui-select');
require('angular-scroll');
require('v-accordion');
window['X2JS'] = require('x2js');
require('angular-xml');
require('angular-sticky-plugin');

import 'lodash';
import 'angular-simple-logger';
import 'angular-google-maps';
import objectFitImages from 'object-fit-images';

objectFitImages(); // object-fit for IE
