<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package olivia
 */

get_header(); ?>

	<ng-include src="'/wp-content/themes/olivia/app/components/main/main.view.html'"></ng-include>
	<div preloader></div>

<?php
get_footer();
